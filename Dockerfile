FROM node:10.15.0-alpine

WORKDIR /usr/src/app

ARG port 
ARG path
ARG cache_url
ARG broker_url
ARG broker_cluster
ARG broker_client_id

ENV PORT=${port} \
    CACHE_URL=${cache_url} \
    BROKER_URL=${broker_url} \
    BROKER_CLUSTER=${broker_cluster} \
    BROKER_CLIENT_ID=${broker_client_id}

EXPOSE ${port}

COPY ${path}/package.json ${path}/package-lock.json ./

RUN npm install

COPY ${path} .

CMD npm run dev 