"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const Post = mongoose_1.model('Post', new mongoose_1.Schema({
    id: String,
    email: String,
    name: String,
}));
exports.default = Post;
//# sourceMappingURL=Post.js.map