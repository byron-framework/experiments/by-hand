"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_koa_1 = require("apollo-server-koa");
const resolvers = {
    Mutation: {
        async createUser(_, args, context, ___) {
            const { db: { User }, stan, uuidv4 } = context;
            const { name, email } = args;
            const user = await User.findOne({ email });
            if (user) {
                throw new apollo_server_koa_1.ApolloError('Email already being user', '409');
            }
            const payload = {
                id: uuidv4(),
                name,
                email,
            };
            const newUserEvent = {
                payload,
                type: 'NewUserCreatedEvent',
            };
            stan.publish('new.user', JSON.stringify(newUserEvent));
            return payload;
        },
    },
};
exports.default = resolvers;
//# sourceMappingURL=resolvers.js.map