"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const resolvers = {
    Mutation: {
        async createPost(_, args, context, ___) {
            const { stan, uuidv4 } = context;
            const { text, ownerId } = args;
            const payload = {
                id: uuidv4(),
                text,
                owner: {
                    id: ownerId,
                },
            };
            const newPostEvent = {
                payload,
                type: 'NewPosetCreatedEvent',
            };
            stan.publish('new.post', JSON.stringify(newPostEvent));
            return payload;
        },
    },
};
exports.default = resolvers;
//# sourceMappingURL=index.js.map