"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_1 = __importDefault(require("koa"));
const cors_1 = __importDefault(require("@koa/cors"));
const apollo_server_koa_1 = require("apollo-server-koa");
const graphql_import_1 = require("graphql-import");
const graphql_tools_1 = require("graphql-tools");
const v4_1 = __importDefault(require("uuid/v4"));
const index_1 = __importDefault(require("./db/models/index"));
const index_2 = __importDefault(require("./resolvers/index"));
const stan_1 = __importDefault(require("./stan"));
console.log(index_2.default);
const port = process.env.PORT ? +process.env.PORT : 3000;
const typeDefs = graphql_import_1.importSchema(`src/graphql/schema.graphql`);
const schema = graphql_tools_1.makeExecutableSchema({ typeDefs, resolvers: index_2.default });
const context = ({ ctx }) => ({
    ctx,
    stan: stan_1.default,
    uuidv4: v4_1.default,
    db: index_1.default,
});
const server = new apollo_server_koa_1.ApolloServer({
    schema,
    context,
});
const app = new koa_1.default();
app.use(cors_1.default());
server.applyMiddleware({ app });
app.listen({ port }, () => {
    console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`);
});
//# sourceMappingURL=server.js.map