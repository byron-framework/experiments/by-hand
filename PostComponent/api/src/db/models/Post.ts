import { Document, model, Model, Schema } from 'mongoose';

import { TUser } from 'src/db/models/User';

export interface PostData {
  id: string;
  text: string;
  owner: {
    id: string;
  };
}

interface Post extends Document {
  id: string;
  text: string;
  owner: TUser;
}

export declare type TPost = Model<Post>;

const Post: TPost = model('Post', new Schema({
  id: String,
  email: String,
  name: String,
}));

export default Post;
