import { Document, model, Model, Schema } from 'mongoose';

export interface UserData {
  id: string;
}

interface User extends Document {
  id: string;
}

export declare type TUser = Model<User>;

const User: TUser = model('User', new Schema({
  id: String,
}));

export default User;
