import mongoose from 'mongoose';

import Post, { TPost } from 'src/db/models/Post';
import User, { TUser } from 'src/db/models/User';

export interface DbConnection {
  [key: string]: TUser | TPost ;
}

const cacheUrl: string = process.env.CACHE_URL
  ? process.env.CACHE_URL
  : 'mongodb://cache:27017/test';

mongoose.connect(cacheUrl);

export default {
  User,
  Post,
};
