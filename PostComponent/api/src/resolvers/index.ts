import { PostData } from 'src/db/models/Post';
import { Context } from 'src/server';

interface CreatePostArgs {
  text: string;
  ownerId: string;
}

const resolvers: any = {
  Mutation: {
    async createPost(_: any, args: CreatePostArgs, context: Context, ___: any): Promise<PostData> {
      const { stan, uuidv4 }: Context = context;
      const { text, ownerId }: any = args;

      const payload: PostData = {
        id: uuidv4(),
        text,
        owner: {
          id: ownerId,
        },
      };

      const newPostEvent: any = {
        payload,
        type: 'NewPosetCreatedEvent',
      };

      stan.publish('new.post', JSON.stringify(newPostEvent));

      return payload;
    },
  },
};

export default resolvers;
