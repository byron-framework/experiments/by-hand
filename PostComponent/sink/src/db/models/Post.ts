import { Document, model, Model, Schema } from 'mongoose';

export interface PostData {
  id: string;
  email: string;
  name: string;
}

interface Post extends Document {
  id: string;
  email: string;
  name: string;
}

export declare type TPost = Model<Post>;

const Post: TPost = model('Post', new Schema({
  id: String,
  email: String,
  name: String,
}));

export default Post;
