import db from 'src/db/models';
import stan from 'src/stan';

stan.on('connect', () => {
  const replayAll: any = stan
    .subscriptionOptions()
    .setDeliverAllAvailable();

  const newUser: any = stan.subscribe('new.user', replayAll);
  const newPost: any = stan.subscribe('new.post', replayAll);

  newUser.on('message', async (msg: any) => {
    const event: any = JSON.parse(msg.getData());

    await db.User.create(event.payload);
  });

  newPost.on('message', async (msg: any) => {
    const event: any = JSON.parse(msg.getData());

    await db.Post.create(event.payload);
  });
});
