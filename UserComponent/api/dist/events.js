"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Event {
    constructor(type, topic, payload) {
        this._type = type;
        this._topic = topic;
        this._payload = payload;
    }
    get topic() {
        return this._topic;
    }
    get type() {
        return this._type;
    }
    serialized() {
        const copy = {
            topic: this._topic,
            payload: this._payload
        };
        return JSON.stringify(copy);
    }
}
class NewUserEvent extends Event {
    constructor(entity) {
        super("NewUserCreated", 'new.user', entity);
    }
}
exports.NewUserEvent = NewUserEvent;
//# sourceMappingURL=events.js.map