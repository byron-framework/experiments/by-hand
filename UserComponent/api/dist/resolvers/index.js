"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_koa_1 = require("apollo-server-koa");
const events_1 = require("../events");
const resolvers = {
    Mutation: {
        async createUser(_, args, context, ___) {
            const { db: { User }, stan, uuidv4 } = context;
            const { name, email } = args;
            let user = await User.findOne({ email });
            if (user) {
                throw new apollo_server_koa_1.ApolloError('Email already being user', '409');
            }
            user = {
                _id: uuidv4(),
                name,
                email,
            };
            const event = new events_1.NewUserEvent(user);
            stan.publish(event.topic, event.serialized());
            return user;
        },
    },
    Query: {
        async getUser(_, args, context, ___) {
            const { db } = context;
            const { _id } = args;
            const user = await db.User.findOne({ _id });
            if (!user)
                throw new apollo_server_koa_1.ApolloError(`Cannot find user with _id ${_id}`, '404');
            return user;
        }
    }
};
exports.default = resolvers;
//# sourceMappingURL=index.js.map