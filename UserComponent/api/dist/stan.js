"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_nats_streaming_1 = require("node-nats-streaming");
const brokerUrl = process.env.BROKER_URL
    ? process.env.BROKER_URL
    : 'nats://broker:4222';
const brokerCluster = process.env.BROKER_CLUSTER
    ? process.env.BROKER_CLUSTER
    : 'test-cluset';
const brokerClientId = process.env.BROKER_CLIENT_ID
    ? process.env.BROKER_CLIENT_ID
    : 'api';
const stan = node_nats_streaming_1.connect(brokerCluster, brokerClientId, {
    url: brokerUrl,
});
exports.default = stan;
//# sourceMappingURL=stan.js.map