import mongoose from 'mongoose';

import User, { TUser } from 'src/db/models/User';

export interface DbConnection {
  [key: string]: TUser;
}

const cacheUrl: string = process.env.CACHE_URL
  ? process.env.CACHE_URL
  : 'mongodb://cache:27017/test';

mongoose.connect(cacheUrl);

export default {
  User,
};
