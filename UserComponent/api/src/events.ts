import { UserData } from 'src/db/models/User';

class Event {
  private _type: string;
  private _topic: string;
  private _payload: any;

  constructor(type: string, topic: string, payload: any) {
    this._type = type;
    this._topic = topic;
    this._payload = payload;
  }

  get topic() {
    return this._topic;
  }

  get type() {
    return this._type;
  }

  serialized(): string {
    const copy: any = {
      topic: this._topic,
      payload: this._payload
    };

    return JSON.stringify(copy);
  }
}

export class NewUserEvent extends Event {
  constructor(entity: UserData) {
    super("NewUserCreated", 'new.user', entity);
  }
}