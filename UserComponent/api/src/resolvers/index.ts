import { ApolloError } from 'apollo-server-koa';
import { UserData } from 'src/db/models/User';
import { Context } from 'src/server';

import { NewUserEvent } from 'src/events';

interface CreateUserArgs {
  name: string;
  email: string;
}

interface GetUserArgs {
  _id: string;
}

const resolvers: any = {
  Mutation: {
    async createUser(_: any, args: CreateUserArgs, context: Context, ___: any): Promise<UserData> {
      const { db: { User }, stan, uuidv4 }: Context = context;
      const { name, email }: any = args;

      let user: any = await User.findOne({ email });

      if (user) {
        throw new ApolloError('Email already being user', '409');
      }

      user = {
        _id: uuidv4(),
        name,
        email,
      };

      const event: NewUserEvent = new NewUserEvent(user);

      stan.publish(event.topic, event.serialized());

      return user;
    },
  },
  Query: {
    async getUser(_: any, args: GetUserArgs, context: Context, ___: any): Promise<UserData> {
      const { db }: Context = context;
      const { _id }: any = args;

      const user: UserData | null =  await db.User.findOne({ _id });
      
      if (!user)
        throw new ApolloError(`Cannot find user with _id ${_id}`, '404');
      
      return user;
    }
  }
};

export default resolvers;
