"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const User_1 = __importDefault(require("./User"));
const cacheUrl = process.env.CACHE_URL
    ? process.env.CACHE_URL
    : 'mongodb://cache:27017/test';
mongoose_1.default.connect(cacheUrl);
exports.default = {
    User: User_1.default,
};
//# sourceMappingURL=index.js.map