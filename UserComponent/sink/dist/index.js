"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const node_nats_streaming_1 = require("node-nats-streaming");
mongoose_1.default.connect('mongodb://cache_user:27017/test');
const User = mongoose_1.model('User', new mongoose_1.Schema({
    id: String,
    email: String,
    name: String,
}));
const stan = node_nats_streaming_1.connect('test-cluster', 'userSink', {
    url: 'nats://broker:4222',
});
stan.on('connect', () => {
    const replayAll = stan.subscriptionOptions().setDeliverAllAvailable();
    const newUser = stan.subscribe('new.user', replayAll);
    newUser.on('message', async (msg) => {
        const event = JSON.parse(msg.getData());
        const user = new User(event.payload);
        await user.save();
    });
});
//# sourceMappingURL=index.js.map