"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = __importDefault(require("./db/models"));
const stan_1 = __importDefault(require("./stan"));
stan_1.default.on('connect', () => {
    const replayAll = stan_1.default
        .subscriptionOptions()
        .setDeliverAllAvailable();
    const newUser = stan_1.default.subscribe('new.user', replayAll);
    newUser.on('message', async (msg) => {
        console.log('-----------------------------------------------');
        const event = JSON.parse(msg.getData());
        return await models_1.default.User.create(event.payload);
    });
});
//# sourceMappingURL=sink.js.map