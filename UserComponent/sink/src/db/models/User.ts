import { Document, model, Model, Schema } from 'mongoose';

export interface UserData {
  _id: string;
  email: string;
  name: string;
}

export interface User extends Document {
  _id: string;
  email: string;
  name: string;
}

export declare type TUser = Model<User>;

const User: TUser = model<User>('User', new Schema({
  _id: String,
  email: String,
  name: String,
}));

export default User;
