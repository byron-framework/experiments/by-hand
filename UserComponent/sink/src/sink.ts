import db from 'src/db/models';
import stan from 'src/stan';

stan.on('connect', () => {
  const replayAll: any = stan
    .subscriptionOptions()
    .setDeliverAllAvailable();

  const newUser: any = stan.subscribe('new.user', replayAll);

  newUser.on('message', async (msg: any) => {
    console.log('-----------------------------------------------');
    const event: any = JSON.parse(msg.getData());

    return await db.User.create(event.payload);
  });
});
